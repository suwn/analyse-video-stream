#pragma once
#include <string>
#include <opencv2/opencv.hpp>         
#include <opencv2/core/version.hpp>
#include <opencv2/videoio/videoio.hpp>
//#include <opencv2/imgcodecs.hpp>
class Utils
{
public:
	static std::string base64Decode(const char* Data, int DataByte);


	static std::string base64Encode(const unsigned char* Data, int DataByte) {
		//编码表
		const char EncodeTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		//返回值
		std::string strEncode;
		unsigned char Tmp[4] = { 0 };
		int LineLength = 0;
		for (int i = 0; i < (int)(DataByte / 3); i++) {
			Tmp[1] = *Data++;
			Tmp[2] = *Data++;
			Tmp[3] = *Data++;
			strEncode += EncodeTable[Tmp[1] >> 2];
			strEncode += EncodeTable[((Tmp[1] << 4) | (Tmp[2] >> 4)) & 0x3F];
			strEncode += EncodeTable[((Tmp[2] << 2) | (Tmp[3] >> 6)) & 0x3F];
			strEncode += EncodeTable[Tmp[3] & 0x3F];
			if (LineLength += 4, LineLength == 76) { strEncode += "\r\n"; LineLength = 0; }
		}
		//对剩余数据进行编码
		int Mod = DataByte % 3;
		if (Mod == 1) {
			Tmp[1] = *Data++;
			strEncode += EncodeTable[(Tmp[1] & 0xFC) >> 2];
			strEncode += EncodeTable[((Tmp[1] & 0x03) << 4)];
			strEncode += "==";
		}
		else if (Mod == 2) {
			Tmp[1] = *Data++;
			Tmp[2] = *Data++;
			strEncode += EncodeTable[(Tmp[1] & 0xFC) >> 2];
			strEncode += EncodeTable[((Tmp[1] & 0x03) << 4) | ((Tmp[2] & 0xF0) >> 4)];
			strEncode += EncodeTable[((Tmp[2] & 0x0F) << 2)];
			strEncode += "=";
		}


		return strEncode;
	}

	//imgType 包括png bmp jpg jpeg等opencv能够进行编码解码的文件
	static std::string Mat2Base64(const cv::Mat& img, std::string imgType) {
		//Mat转base64
		std::string img_data;
		std::vector<uchar> vecImg;
		std::vector<int> vecCompression_params;
		vecCompression_params.push_back(cv::ImwriteFlags::IMWRITE_JPEG_QUALITY);
		vecCompression_params.push_back(90);
		imgType = "." + imgType;
		cv::imencode(imgType, img, vecImg, vecCompression_params);
		img_data = base64Encode(vecImg.data(), vecImg.size());
		return img_data;
	}


	static cv::Mat Base2Mat(std::string& base64_data) {
		cv::Mat img;
		std::string s_mat;
		s_mat = base64Decode(base64_data.data(), base64_data.size());
		std::vector<char> base64_img(s_mat.begin(), s_mat.end());
		img = cv::imdecode(base64_img, cv::ImreadModes::IMREAD_COLOR);
		return img;
	}
};

